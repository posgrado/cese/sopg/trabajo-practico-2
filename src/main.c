/****************************************************************************
 * @file main.c
 * @author David Broin <davidmbroin@gmail.com>
 * @date 2020/08/17
 * @brief Read from socket and serial port
 * @copyright All rights reserved.
 * @copyright @ref [MIT LICENSE](LICENSE.txt)
 ****************************************************************************/

/****************************************************************************
*  Inclusions of public function dependencies                               *
*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "SerialManager.h"
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>

/****************************************************************************
*	Definition macros of private constants                                  *
*****************************************************************************/

#define PORT 10000

/****************************************************************************
*	Definitions of private global variables                                  *
*****************************************************************************/

static volatile sig_atomic_t isRunningMain;
static bool isRunningUART;

/****************************************************************************
*	Prototypes (declarations) of private functions                          *
*****************************************************************************/

static void SigIntListener();
static void SigTermListener();
static void* readUART (void* newfd);

/****************************************************************************
*  Main function, program entry point                                       *
*****************************************************************************/

int main(void)
{

    struct sigaction SigIntAction;
    struct sigaction SigTermAction;
	char sendBuf[] = ">OUTS:1,1,1,1\r\n";

	socklen_t addr_len;
	struct sockaddr_in clientaddr;
	struct sockaddr_in serveraddr;
	char buffer[128];
	int newfd;
	int n;

	pthread_t readUARTTh;

    SigIntAction.sa_handler = &SigIntListener;
    SigIntAction.sa_flags = 0;
    sigemptyset(&SigIntAction.sa_mask);

    if(sigaction(SIGINT, &SigIntAction, NULL)){
		perror("Error asignando listener para SIGINT");
		exit(EXIT_FAILURE);
	}

    SigTermAction.sa_handler = &SigTermListener;
    SigTermAction.sa_flags = 0;
    sigemptyset(&SigTermAction.sa_mask);

    if(sigaction(SIGTERM, &SigTermAction, NULL)){
		perror("Error asignando listener para SIGTERM");
		exit(EXIT_FAILURE);
	}
    
	printf("Inicio Serial Service\r\n");
	printf("Intentando abrir puerto /dev/ttyUSB1\r\n");
	while(serial_open(1, 115200)){
		perror("Error al abrir puerto serie\r\n");
		printf("Reintentando en 5 segundos...\r\n");
		usleep(5000000);
	}
	printf("Puerto serie abierto, comenzando configuración de red\r\n");
	
	// Creamos socket
	int s = socket(AF_INET,SOCK_STREAM, 0);

	// Cargamos datos de IP:PORT del server
	bzero((char*) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(PORT);
	if(inet_pton(AF_INET, "127.0.0.1", &(serveraddr.sin_addr))<=0)
	{
		perror("ERROR invalid server IP");
		exit(EXIT_FAILURE);
	}

	// Abrimos puerto con bind()
	if (bind(s, (struct sockaddr*)&serveraddr, sizeof(serveraddr)) == -1) {
		close(s);
		perror("Listener: bind");
		exit(EXIT_FAILURE);
	}

	// Seteamos socket en modo Listening
	if (listen (s, 10) == -1) // backlog=10
  	{
		perror("Error en listen");
		close(s);
		exit(EXIT_FAILURE);
  	}

	isRunningMain = true;
	while(isRunningMain){
		printf("Servicio configurado, esperando conexiones entrantes en puerto %d...\r\n", PORT);
		addr_len = sizeof(struct sockaddr_in);
		if ( -1 == (newfd = accept(s, (struct sockaddr *)&clientaddr, &addr_len)))
		{
			if(isRunningMain){
				perror("Error en accept");
				close(newfd);
				close(s);
				exit(EXIT_FAILURE);
			} else {
				printf("Llegó una señal de cierre\r\n");
			}
		} else {
			char ipClient[32];
			inet_ntop(AF_INET, &(clientaddr.sin_addr), ipClient, sizeof(ipClient));
			printf("Conexion entrante desde:  %s\r\n", ipClient);
			printf("Creando hilo para leer puerto serie\r\n");
			pthread_create (&readUARTTh, NULL, readUART, (void *) &newfd);
		}
		while(isRunningMain){
			n = (int) read(newfd,buffer,128);
			if( -1 == n ){
				if(isRunningMain){
					perror("Error leyendo mensaje en socket");
					close(newfd);
					close(s);
					exit(EXIT_FAILURE);
				} else {
					printf("Llegó una señal de cierre\r\n");
				}
			} else if( 0 < n ) {
				buffer[n]=0x00;
				printf("Recibi %d bytes por TCP: %s\r\n",n,buffer);
				//:STATES0000
				//">OUTS:1,1,1,1\r\n"
				sendBuf[6] = buffer[7];
				sendBuf[8] = buffer[8];
				sendBuf[10] = buffer[9];
				sendBuf[12] = buffer[10];
				serial_send(sendBuf, sizeof(sendBuf));
			} else {
				printf("Cliente desconectado, se regresa a aceptar conexiones\r\n");
				isRunningUART = false;
				close(newfd);
				break;
			}
			usleep(100000);
		}
	}
	printf("Esperando a que termine el hilo readUARTTh\r\n");
	pthread_join(readUARTTh, NULL);
	printf("Cerrando archivos abiertos\r\n");
	close(newfd);
	close(s);
	printf("Bye Bye from main thread (socket reading)\r\n");
	exit(EXIT_SUCCESS);
}

/****************************************************************************
*	Implementation of private functions                                     *
*****************************************************************************/

static void SigIntListener(){
    isRunningMain = false;
    printf("SIGINT arrived\r\n");
}
static void SigTermListener(){
    isRunningMain = false;
    printf("SIGTERM arrived\r\n");
}

static void* readUART (void* newfd){
	char recieveBuf[1024];
	char socketTXBuf[] = ":LINEXTG\n";
	int n;

	isRunningUART = true;
	while(isRunningMain && isRunningUART){
		n = serial_receive(recieveBuf, sizeof(recieveBuf));
		if(0 < n){
			recieveBuf[n]=0x00;
			printf("Recibi por UART %d bytes.: %s\r\n", n, recieveBuf);
			//Verificacion simple para identificar paquete
			if('T' == recieveBuf[1]){
				//>TOGGLE STATE:0
				//:LINEXTG\n
				socketTXBuf[5] = recieveBuf[14];
				if (-1 == write (*(int *) newfd, socketTXBuf, sizeof(socketTXBuf)))
				{
					perror("Error escribiendo mensaje en socket");
					exit(EXIT_FAILURE);
				}
			}
		}
		usleep(100000);
	}
	printf("Bye Bye from UART reading thread\r\n");
	return NULL;
}
